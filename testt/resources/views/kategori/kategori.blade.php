@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Kategori
		<div class="pull-right">
			Tambah Data <a href="{{ url('kategori/tambah')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>ID</td>
					<td>Deskripsi</td>
				</tr>
				@foreach($kategori as $kategories)
					
				<tr>
					<td >{{ $kategories->id}}</td>
					<td >{{ $kategories->deskripsi}}</td>
					<td >
					
					<a href="{{url('kategori/edit/'.$kategories->id)}}"><img src="pensil.png" height="20"></img></a>
					<a href="{{url('kategori/hapus/'.$kategories->id)}}"><img src="sampah.ico" height="20"></img></a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

