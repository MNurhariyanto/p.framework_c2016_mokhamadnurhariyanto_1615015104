<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli';

    public function Pengguna()
    {
		return $this->BelongsTo(Pengguna::class);
	}
}
