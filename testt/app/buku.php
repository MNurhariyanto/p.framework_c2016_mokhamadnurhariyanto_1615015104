<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['judul','penerbit','tanggal','kategori_id'];

    public function Kategori()
    {
		return $this->belongsTo(Kategori::class);
	}

	public function Penulis()
	{
		return $this->belongsToMany(Penulis::class);
	}

}
