<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku_Penulis extends Model
{
    protected $table = 'buku_penulis';
    protected $fillable = ['penulis_id','buku_id'];

    public function Buku()
    {
    	return $this->belongsToMany(Buku::class);
    }
    public function Penulis()
    {
    	return $this->belongsToMany(Penulis::class);
    }
}
